# 16路舵机控制板

此舵机驱动板使用PCA9685芯片，是16通道12bit PWM舵机驱动，用2个引脚通过I2C就可以驱动16个舵机。不仅如此，你还可以通过级联的方式最多级联62个驱动板，总共可以驱动992个舵机!

![](./arduinoC/_images/featured.png)

# 积木

**特别注意**

级联的每个驱动板都需要有一个唯一的访问地址。每个驱动板的初始I2C地址是0×40，可以通过右上角的跳线修改I2C地址。用焊锡将一个跳线连上就表示一个二进制数字“1”。

![](./arduinoC/_images/note.png)

Board 0:  Address = 0×40 Offset = binary 00000 (默认)

Board 1:  Address = 0×41  Offset = binary 00001 (如上图，接上A0)

Board 2:  Address = 0×42  Offset = binary 00010 (接上A1)

Board 3:  Address = 0×43  Offset = binary 00011 (接上A0和A1)

Board 4:  Address = 0×44  Offset = binary 00100 (接上A2)

以此类推。。。

![](./arduinoC/_images/blocks.png)

# 示例程序


![](./arduinoC/_images/example.png)

![](./arduinoC/_images/example1.png)



 

# 支持列表

|主板型号|实时模式|ArduinoC|MicroPython|备注|
|-----|-----|:-----:|-----|-----|
|uno||√|||
|micro:bit||√|||
|mpython||√|||
|leonardo||√|||
|nano:bit||√|||
|mega2560||√|||


# 更新日志

V0.0.1 基础功能完成
V0.0.2 去除串口打印的数据

