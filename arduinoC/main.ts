//% color="#4169E1" iconWidth=50 iconHeight=40
namespace ServoPCA9685{
    //% block="16ServoPCA9685 Init BOARD [BOARD]ADDRESS [ADDRESS]FREQ [FREQ]   " blockType="command"
    //% BOARD.shadow="dropdown" BOARD.options="BOARD" 
    //% ADDRESS.shadow="string" ADDRESS.defl="0x40" 
    //% FREQ.shadow="number" FREQ.defl="50" 
    export function ServoPCA9685Init(parameter: any, block: any) {      
      let board=parameter.BOARD.code; 
      let address=parameter.ADDRESS.code; 
      let freq=parameter.FREQ.code;  
      
      Generator.addInclude("16ServoPCA9685","#include <Adafruit_PWMServoDriver.h>");

      Generator.addObject(`16ServoPCA9685${board}` ,`Adafruit_PWMServoDriver`,`pwm${board} = Adafruit_PWMServoDriver(${address.replace("\"","").replace("\"","")});`);  
      
      Generator.addSetup(`16ServoPCA9685`,`pwm${board}.begin();`);
      
      Generator.addSetup(`16ServoPCA9685${board}`,`pwm${board}.setPWMFreq(${freq});`);
       
  }
    //% block="16ServoPCA9685 Board [BOARD]PIN [PIN]ANGLE [ANGLE] " blockType="command"
    //% BOARD.shadow="dropdown" BOARD.options="BOARD" 
    //% PIN.shadow="dropdownRound" PIN.options="PIN"    
    //% ANGLE.shadow="number"  ANGLE.defl="90" 
    export function ServoPCA9685Angle(parameter: any, block: any) {        
      let board=parameter.BOARD.code;
      let pin=parameter.PIN.code;         
      let angle=parameter.ANGLE.code;
 
        Generator.addCode(`pwm${board}.writeServo(${pin},${angle});`);
      
            
  }
    
}